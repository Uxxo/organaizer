import months.Event;
import months.Month;

import java.io.*;
import java.util.*;

public class FileManager {


    private File passwordFile;
    private String passwordFileName;
    private File databaseFile;
    private String dataBaseFileName;


    public FileManager() {
        passwordFileName = "password.txt";
        dataBaseFileName = "database.txt";
        passwordFile = new File(passwordFileName);
        databaseFile = new File(dataBaseFileName);

        if (!passwordFile.exists()) {
            try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(passwordFile))) {
                passwordFile.createNewFile();
                passwordFile.mkdir();
            } catch (IOException ex) {
                ex.printStackTrace();
            }

        }

        if (!databaseFile.exists()) {
            try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(databaseFile))) {
                databaseFile.createNewFile();
                passwordFile.mkdir();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    public void createPassword(int passwordHashcode) {

        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(passwordFile))) {
            passwordFile.createNewFile();
            passwordFile.mkdir();

            bufferedWriter.write(String.valueOf(passwordHashcode));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



    public String readPassword(String address) {
        String passwordString = null;
        int password = 0;
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(passwordFile))) {
            passwordFile.createNewFile();
            while ((passwordString = bufferedReader.readLine()) != null) {
//                passwordString
                return passwordString;
            }
        } catch (IOException e) {
            System.out.println("game over");
        }
        return passwordString;
    }

    public void createDataBaseFile(Map<String, Month> mountMap) {


        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(databaseFile))) {
            Set<String> keys = mountMap.keySet();
            for (String s : keys) {
                Month month = mountMap.get(s);
                bufferedWriter.write(month.getMonthName() + ":");
                bufferedWriter.write(month.getMaxDate() + ":");
                Set<Integer> events = month.getEventMap().keySet();
                for(Integer i:events){
                    Event event = month.getEventMap().get(i);
                    bufferedWriter.write(i + "I");
                    bufferedWriter.write(event.getType() + "I");
                    bufferedWriter.write(event.getMembers() + "I");
                    bufferedWriter.write(event.getStartedTime() + "I");
                    bufferedWriter.write(event.getAddress() + "I");
                    bufferedWriter.write(event.getCost() + "I");
                    bufferedWriter.write(event.getSpecialInfo() + "I");
                    bufferedWriter.write(i + "END");
                }
                bufferedWriter.write( "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Map<String, Month> readDataBaseFile(Map <String, Month> oldMonthsMap) {

        Map<String,Month> newMonthsMap = new HashMap<>();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(databaseFile))) {



            String line = null;
            while ((line = bufferedReader.readLine()) != null) {

                String[] words = line.split(":".trim());
                if(words[0].contains(" ")){
                    return oldMonthsMap;
                }

                String monthName = words[0];
                String monthMaxDate = words[1];
                Month month = new Month(monthName,Integer.parseInt(monthMaxDate));

                if(words.length>2){
                    String [] eventsArray = words[2].split("END".trim());
                    for (String eventsString: eventsArray){
                        String[] eventValues = eventsString.split("I".trim());
                        String evenDate = eventValues[0];
                        String eventType = eventValues[1];
                        String eventMembers = eventValues[2];
                        String eventTimeStarted = eventValues[3];
                        String eventAddress = eventValues[4];
                        String eventCost = eventValues[5];
                        String eventSpecialInfo = eventValues[6];

                        Event event = new Event();
                        event.setType(eventType);
                        event.setMembers(eventMembers);
                        event.setStartedTime(eventTimeStarted);
                        event.setAddress(eventAddress);
                        event.setCost(eventCost);
                        event.setSpecialInfo(eventSpecialInfo);

                        month.getEventMap().put(Integer.parseInt(evenDate),event);

                    }

                }
                newMonthsMap.put(monthName.toUpperCase(),month);

            }

            if (newMonthsMap.size() == 0){
                return oldMonthsMap;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return newMonthsMap;
    }

    public String getPasswordFileName() {
        return passwordFileName;
    }

}
