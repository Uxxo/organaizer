import comparators.ComparatorForInt;
import exceptions.InvalidDateException;
import months.Event;
import months.EventManager;
import months.Month;
import months.MonthsManager;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import java.util.List;
import java.util.concurrent.CountDownLatch;

public class View {

    EventManager eventManager = new EventManager();
    FileManager fileManager = new FileManager();
    MonthsManager monthsManager = new MonthsManager();


    private JFrame frame;
//    private JLabel label;
//    private JLabel invalidPasswordLabel;
//    private JTextField textField;

    //    private JButton passwordButton;
    private boolean flag = false;
    private String mount;
    private String input;
    private int valueMenu;


    public View() {
        frame = new JFrame();

        frame.setSize(400, 750);
        frame.setLayout(null);
        frame.setResizable(false);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

    }


    public boolean fillOnPasswordForm() {

        CountDownLatch latch = new CountDownLatch(1);
        flag = false;

        frame.setTitle("Сохранение пароля");
//        frame.setSize(400, 750);
//        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        JPanel panel = new JPanel();
        panel.setSize(400, 720);
        panel.setBackground(Color.LIGHT_GRAY);
        panel.setLayout(null);

        JLabel lblAnswer = new JLabel("");


        frame.add(panel);

        Font tnr = new Font("Times new roman", Font.TYPE1_FONT, 17);
        JLabel lblMessage = new JLabel("Нужно сохранить пароль,");
        lblMessage.setBounds(10, 15, 380, 40);
        JLabel lblMessage2 = new JLabel("чтобы защитить Ваши данные");
        lblMessage2.setBounds(10, 35, 380, 40);
        lblMessage.setFont(tnr);
        lblMessage2.setFont(tnr);

        JLabel lblEnterPassword = new JLabel("Введите пароль");
        lblEnterPassword.setBounds(10, 80, 380, 40);

        JTextField tfdPassword1 = new JTextField(4);
        tfdPassword1.setBounds(145, 87, 80, 25);

        JLabel lblRepeatPassword = new JLabel("Повторите пароль");
        lblRepeatPassword.setBounds(10, 140, 380, 40);

        JTextField tfdPassword2 = new JTextField(4);
        tfdPassword2.setBounds(145, 147, 80, 25);

        lblAnswer.setBounds(65, 290, 300, 40);
        lblAnswer.setFont(tnr);

        JButton btnSavePassword = new JButton("Ввод");
        btnSavePassword.setBounds(90, 400, 200, 40);

        JButton btnOnceAgain = new JButton("Попробовать еще раз");
        btnOnceAgain.setBounds(90, 400, 200, 40);

        panel.add(lblMessage);
        panel.add(lblMessage2);
        panel.add(lblEnterPassword);
        panel.add(tfdPassword1);
        panel.add(lblRepeatPassword);

        panel.add(tfdPassword1);
        panel.add(tfdPassword2);
        panel.add(lblAnswer);
        panel.add(btnSavePassword);
//        panel.add(btnOnceAgain);

        frame.revalidate();
        frame.repaint();
        frame.setVisible(true);


        btnSavePassword.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                String password = tfdPassword1.getText().trim();
                char[] chars = password.toCharArray();


                if (chars.length == 0) {
                    lblAnswer.setText("          Нужно ввести пароль!");


                    panel.remove(tfdPassword1);
                    panel.remove(tfdPassword2);
                    panel.remove(btnSavePassword);
                    panel.add(btnOnceAgain);
                    frame.revalidate();
                    frame.repaint();

                } else if (tfdPassword1.getText().hashCode() == tfdPassword2.getText().hashCode()) {

                    fileManager.createPassword(tfdPassword1.getText().hashCode());
                    frame.remove(panel);
                    flag = true;
                    frame.revalidate();
                    frame.repaint();
                    latch.countDown();

                } else {
                    lblAnswer.setText("Введенные пароли не совпадают!");
//                    tfdPassword1.setText("");
//                    tfdPassword2.setText("");
                    panel.remove(tfdPassword1);
                    panel.remove(tfdPassword2);
                    panel.remove(btnSavePassword);
                    panel.add(btnOnceAgain);
                    frame.revalidate();
                    frame.repaint();

                }
            }
        });

        btnOnceAgain.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                frame.remove(panel);
                latch.countDown();
            }
        });
        panel.add(lblAnswer);
        frame.setVisible(true);
        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return flag;
    }


    public String singIn() {
        CountDownLatch latch = new CountDownLatch(1);

        Font tnr = new Font("Times new roman", Font.TYPE1_FONT, 17);
        frame.setTitle("Авторизация");
        frame.setSize(400, 750);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        JPanel panel = new JPanel();
        panel.setSize(400, 720);
        panel.setBackground(Color.LIGHT_GRAY);
        panel.setLayout(null);

        JLabel lblEnterPassword = new JLabel("Введите пароль");
        lblEnterPassword.setFont(tnr);
        lblEnterPassword.setBounds(10, 15, 380, 40);

        JTextField tfdPassword = new JTextField(6);
        tfdPassword.setBounds(145, 80, 80, 20);

        JButton btnPassword = new JButton("Войти");
        btnPassword.setBounds(90, 400, 200, 40);

        flag = false;
        panel.add(lblEnterPassword);
        panel.add(tfdPassword);
        panel.add(btnPassword);

        panel.setVisible(true);
        frame.add(panel);
        frame.setVisible(true);
        btnPassword.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                input = tfdPassword.getText();
                latch.countDown();
                flag = true;
            }
        });
        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        frame.revalidate();
        frame.remove(panel);
        return input;
    }// вход

    public void showMessageInvalidPassword() {

        JPanel panel2 = new JPanel();
        Font tnr = new Font("Times new roman", Font.TYPE1_FONT, 20);

        panel2.setBounds(0, 200, 400, 70);

        panel2.setBackground(Color.GRAY);
        panel2.setLayout(new GridLayout(1, 0));
        JLabel lblInvalidPassword = new JLabel("                      Неверный пароль!");
        lblInvalidPassword.setFont(tnr);
        lblInvalidPassword.setBackground(Color.white);
        lblInvalidPassword.setBounds(80, 5, 300, 40);

        panel2.add(lblInvalidPassword);
        panel2.setVisible(true);
        frame.add(panel2);
//        frame.revalidate();
//        frame.repaint();


    }

    public String start() {

        CountDownLatch latch = new CountDownLatch(1);

        Map<String, Month> refreshedMapBusyDates = fileManager.readDataBaseFile(monthsManager.getMonthsMap());
        monthsManager.refreshMonthsMap(refreshedMapBusyDates);

        frame.setTitle("Organizer");
        frame.setSize(400, 750);


        JPanel panel = new JPanel();
        panel.setSize(400, 710);
        panel.setBackground(Color.LIGHT_GRAY);
        frame.add(panel);
        panel.setLayout(new GridLayout(0, 2));

        String[] monsArray = new String[]{"Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август",
                "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"};

        for (String s : monsArray) {
            JButton btnMont = new JButton(s);
            JLabel lbtBusyDate = new JLabel(printBusyDates(s));

            panel.add(btnMont);
            panel.add(lbtBusyDate);

            btnMont.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent actionEvent) {
                    mount = btnMont.getText().toUpperCase();
                    latch.countDown();


                }
            });
        }

        frame.setVisible(true);

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        frame.remove(panel);
        frame.revalidate();
        frame.repaint();
        return mount;
    }

    public int getMount(String mountsName) {
        valueMenu = 0;
        CountDownLatch latch = new CountDownLatch(1);


        Month month = monthsManager.readMount(mountsName);
        Map<Integer, Event> eventMap = month.getEventMap();

        Font tnr = new Font("Times new roman", Font.TYPE1_FONT, 20);
        frame.setTitle(mountsName);
        JPanel panel = new JPanel();
        panel.setSize(400, 715);
        panel.setBackground(Color.LIGHT_GRAY);
        panel.setLayout(null);

        JPanel panelDate = new JPanel();
        panelDate.setBounds(0, 100, 390, 80);
        panelDate.setBackground(Color.LIGHT_GRAY);

        panelDate.setLayout(new FlowLayout());

        JLabel lblBusyDate = new JLabel("Занятые даты:");
        lblBusyDate.setBounds(10, 15, 380, 40);
        lblBusyDate.setFont(tnr);

        panel.add(lblBusyDate);
        panel.add(panelDate);

        Set<Integer> eventDates = eventMap.keySet();
        Comparator<Integer> comparator = new Comparator<Integer>() {
            @Override
            public int compare(Integer i1, Integer i2) {
                return i1 - i2;
            }
        };
        List<Integer> dates = new ArrayList();
        for (Integer i : eventDates) {
            dates.add(i);
        }
        Collections.sort(dates, comparator);
        for (int i : dates) {
            JButton button = new JButton(String.valueOf(i));
            panelDate.add(button);
            button.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent actionEvent) {

//                    openEvent(mount, Integer.parseInt(button.getText()));
                    valueMenu = Integer.parseInt(button.getText());
                    frame.remove(panel);
                    latch.countDown();
                }
            });
        }

        JButton btnAddDate = new JButton("Добавить дату");
        btnAddDate.setBounds(90, 450, 200, 40);

        btnAddDate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                valueMenu = 100;
//                addDate(mount.getMountName());
                frame.remove(panel);
                frame.remove(panelDate);
                latch.countDown();
            }
        });

        panel.add(btnAddDate);

        JButton jbtReturn = new JButton("Назад");
        jbtReturn.setBounds(90, 491, 200, 40);
        jbtReturn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                valueMenu = 0;
                panel.setVisible(false);
//                frame.remove(panel);
                latch.countDown();
            }
        });
        panel.add(jbtReturn);
        frame.add(panel);
        frame.revalidate();
        frame.repaint();
        frame.setVisible(true);
        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return valueMenu;
    }

    public int addDate(String mountsName) {
        valueMenu = 0;

        CountDownLatch latch = new CountDownLatch(1);

        Font tnr = new Font("Times new roman", Font.TYPE1_FONT, 20);
        JPanel panel = new JPanel();
        panel.setSize(400, 710);
        panel.setBackground(Color.LIGHT_GRAY);
        frame.add(panel);
        panel.setLayout(null);


        JLabel lblAddDate = new JLabel("Введите дату:");
        lblAddDate.setBounds(10, 15, 380, 40);
        lblAddDate.setFont(tnr);

        JTextField tflNewDate = new JTextField(2);
        tflNewDate.setBounds(147, 80, 80, 20);
        JButton btnAddDate = new JButton("Добавить");
        btnAddDate.setBounds(90, 450, 200, 40);
        JButton btnReturn = new JButton("Назад");
        btnReturn.setBounds(90, 491, 200, 40);

        JLabel lblMessage = new JLabel("");
        lblMessage.setFont(tnr);
        lblMessage.setBounds(120, 100, 380, 40);


        btnAddDate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try {

                    monthsManager.addDate(mountsName, Integer.parseInt(tflNewDate.getText()));
                    fileManager.createDataBaseFile(monthsManager.getMonthsMap());
                    lblMessage.setText("Дата добавлена!");
                    lblMessage.setBounds(120, 100, 380, 40);
                    JButton btnOk = new JButton("Ok");
                    btnOk.setBounds(90, 450, 200, 40);
                    panel.remove(btnAddDate);
                    panel.remove(lblAddDate);
                    panel.remove(tflNewDate);
                    panel.remove(btnReturn);
                    panel.add(btnOk);
                    frame.revalidate();
                    frame.repaint();
                    btnOk.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent actionEvent) {
                            valueMenu = Integer.parseInt(tflNewDate.getText());
//                                frame.remove(panel);
//                                panel.setVisible(false);

                            latch.countDown();
                        }
                    });

                } catch (InvalidDateException e) {


                    lblMessage.setText("Дата некорректна, или уже используется!");
                    lblMessage.setBounds(1, 100, 399, 40);

                }
            }
        });

        btnReturn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                frame.remove(panel);
                latch.countDown();
            }
        });

        panel.add(lblAddDate);
        panel.add(tflNewDate);
        panel.add(btnAddDate);
        panel.add(btnReturn);
        panel.add(lblMessage);

        frame.revalidate();
        frame.repaint();
        frame.setVisible(true);
        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        panel.setVisible(false);
        return valueMenu;
    }

    public int createNewEvent(String mountName, int date) {

        valueMenu = 0;

        CountDownLatch latch = new CountDownLatch(1);
        monthsManager.refreshMonthsMap(monthsManager.getMonthsMap());

        Month month = monthsManager.readMount(mountName);

        frame.setTitle("Добавление мероприятия");
        JPanel panel = new JPanel();
        panel.setBackground(Color.LIGHT_GRAY);
        panel.setSize(400, 700);
        panel.setLayout(new GridLayout(0, 1));

        frame.add(panel);
        Event event;

        JLabel type = new JLabel();
        JLabel members = new JLabel();
        JLabel startedTime = new JLabel();
        JLabel address = new JLabel();
        JLabel cost = new JLabel();
        JLabel specialInfo = new JLabel();

        JButton btnAddEvent = new JButton("Сохранить");


        type.setText("Добавить мероприятие");
        JTextField typeText = new JTextField(15);
        members.setText("Добавить участников");
        JTextField membersText = new JTextField(30);
        startedTime.setText("Добавить время начала");
        JTextField timeText = new JTextField(15);
        address.setText("Добавить адресс");
        JTextField addressText = new JTextField(15);
        cost.setText("Добавить стоимость");
        JTextField costText = new JTextField(15);
        specialInfo.setText("Добавить дополнительную информацию");
        JTextField infoText = new JTextField(15);


        btnAddEvent.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Event newEvent = eventManager.createEvent(typeText.getText(),
                        membersText.getText(), timeText.getText(),
                        addressText.getText(), costText.getText(), infoText.getText());

                Map<Integer, Event> eventMap = month.getEventMap();
                eventMap.put(date, newEvent);
                month.setEventMap(eventMap);
                monthsManager.getMonthsMap().put(month.getMonthName(), month);
                fileManager.createDataBaseFile(monthsManager.getMonthsMap());


                frame.remove(panel);
                latch.countDown();
            }
        });


        panel.add(type);
        panel.add(typeText);
        panel.add(members);
        panel.add(membersText);
        panel.add(startedTime);
        panel.add(timeText);
        panel.add(address);
        panel.add(addressText);
        panel.add(cost);
        panel.add(costText);
        panel.add(specialInfo);
        panel.add(infoText);

        panel.add(btnAddEvent);


        frame.revalidate();
        frame.repaint();
        frame.setVisible(true);


        frame.revalidate();
        frame.repaint();
        frame.setVisible(true);
        try {
            latch.await();
        } catch (
                InterruptedException e) {
            e.printStackTrace();
        }

        return valueMenu;

    }

    public int openEvent(String mountName, int date) {

        valueMenu = 0;

        CountDownLatch latch = new CountDownLatch(1);
        monthsManager.refreshMonthsMap(monthsManager.getMonthsMap());

        Month month = monthsManager.readMount(mountName);

        frame.setTitle(mountName + ": " + date);

        JPanel panel = new JPanel();
        panel.setSize(400, 710);
        panel.setBackground(Color.LIGHT_GRAY);
        panel.setLayout(new GridLayout(0, 1));

        frame.add(panel);
        Event event;

        JLabel type = new JLabel();
        JLabel members = new JLabel();
        JLabel startedTime = new JLabel();
        JLabel address = new JLabel();
        JLabel cost = new JLabel();
        JLabel specialInfo = new JLabel();

        JButton btnAddEvent = new JButton("Добавить информацию.");
        JButton btnEdit = new JButton("Редактировать");
        JButton btnReturn = new JButton("Назад");


        event = month.getEventMap().get(date);

        if (event.getType().equals("") && event.getMembers().equals("")) {
            frame.remove(panel);
            valueMenu = 1;
            return valueMenu;
        }

        type.setText(event.getType());
        members.setText(event.getMembers());
        startedTime.setText(event.getStartedTime());
        address.setText(event.getAddress());
        cost.setText(event.getCost());
        specialInfo.setText(event.getSpecialInfo());

        System.out.println(event.toString());

        panel.add(type);
        panel.add(members);
        panel.add(startedTime);
        panel.add(address);
        panel.add(cost);
        panel.add(specialInfo);

        panel.add(btnEdit);
        panel.add(btnReturn);


        btnReturn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                valueMenu = 0;
                frame.remove(panel);
                latch.countDown();

            }
        });

        btnEdit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {

                valueMenu = 1;
                frame.remove(panel);
                latch.countDown();
            }
        });

        frame.revalidate();
        frame.repaint();
        frame.setVisible(true);
        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return valueMenu;
    }

    public int editEvent(String monthName, int date) {

        valueMenu = 0;

        CountDownLatch latch = new CountDownLatch(1);

        Month month = monthsManager.readMount(monthName);

        frame.setTitle(monthName + ": " + date);

        JPanel panel = new JPanel();
        panel.setSize(400, 700);
        panel.setLayout(new GridLayout(0, 1));

        frame.add(panel);
        Event event = month.getEventMap().get(date);

        JLabel lblType = new JLabel("Редактировать тип");
        JTextField jtfType = new JTextField();
        jtfType.setText(event.getType());

        JLabel lblMembers = new JLabel("Редактировать участников");
        JTextField jtfMembers = new JTextField();
        jtfMembers.setText(event.getMembers());

        JLabel lblStartedTime = new JLabel("Редактировать время начала");
        JTextField jtfStartedTime = new JTextField();
        jtfStartedTime.setText(event.getStartedTime());

        JLabel lblAddress = new JLabel("Редактировать адрес");
        JTextField jtfAddress = new JTextField();
        jtfAddress.setText(event.getAddress());

        JLabel lblCost = new JLabel("Редактировать стоимость");
        JTextField jtfCost = new JTextField();
        jtfCost.setText(event.getCost());

        JLabel lblSpecialInfo = new JLabel("Редактировать информацию");
        JTextField jtfSpecialInfo = new JTextField();
        jtfSpecialInfo.setText(event.getSpecialInfo());

        JButton btnSaveEvent = new JButton("Сохранить");
        JButton btnDeleteEvent = new JButton("Удалить");
        JButton btnReturn = new JButton("Назад");

        panel.add(lblType);
        panel.add(jtfType);
        panel.add(lblMembers);
        panel.add(jtfMembers);
        panel.add(lblStartedTime);
        panel.add(jtfStartedTime);
        panel.add(lblAddress);
        panel.add(jtfAddress);
        panel.add(lblCost);
        panel.add(jtfCost);
        panel.add(lblSpecialInfo);
        panel.add(jtfSpecialInfo);

        panel.add(btnSaveEvent);
        panel.add(btnDeleteEvent);
        panel.add(btnReturn);


        btnSaveEvent.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Event newEvent = eventManager.createEvent(jtfType.getText(),
                        jtfMembers.getText(), jtfStartedTime.getText(),
                        jtfAddress.getText(), jtfCost.getText(), jtfSpecialInfo.getText());

                Map<Integer, Event> eventMap = month.getEventMap();
                eventMap.put(date, newEvent);
                month.setEventMap(eventMap);

//                mount.getEventMap().put(date, newEvent);
                frame.remove(panel);
                valueMenu = 0;
                latch.countDown();
            }
        });


        btnReturn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                valueMenu = 0;
                frame.remove(panel);
                latch.countDown();
            }
        });

        btnDeleteEvent.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                valueMenu = -1;
                frame.remove(panel);
                latch.countDown();
            }
        });

        frame.revalidate();
        frame.repaint();
        frame.setVisible(true);


        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("возвращаю" + valueMenu);
        return valueMenu;
    }

    public int deleteEvent(String monthName, int date) {

        valueMenu = 0;

        CountDownLatch latch = new CountDownLatch(1);

        Month month = monthsManager.readMount(monthName);

        frame.setTitle("Удаление мероприятия");
        Font tnr = new Font("Times new roman", Font.TYPE1_FONT, 20);

        JPanel panel = new JPanel();
        panel.setSize(400, 700);
        panel.setBackground(Color.LIGHT_GRAY);
        panel.setLayout(null);

        frame.add(panel);

        Event event = month.getEventMap().get(date);

        JLabel lblDelete = new JLabel("Удалить мероприятие ");
        lblDelete.setBounds(10, 20, 380, 40);
        lblDelete.setFont(tnr);

        JLabel lblEvent = new JLabel(monthName + ": " + date);
        lblEvent.setBounds(120, 100, 380, 40);

        JButton btnDelete = new JButton("Удалить");
        btnDelete.setBounds(90, 450, 200, 40);


        JButton btnCancel = new JButton("Отмена");
        btnCancel.setBounds(90, 491, 200, 40);

        lblEvent.setFont(tnr);

        panel.add(lblDelete);
        panel.add(lblEvent);
        panel.add(btnDelete);
        panel.add(btnCancel);

        btnDelete.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {

                panel.setVisible(false);
                JPanel panel2 = new JPanel();
                panel2.setSize(400, 710);
                panel2.setBackground(Color.LIGHT_GRAY);
                panel2.setLayout(null);
                panel2.setBackground(Color.LIGHT_GRAY);

                JLabel lblMessage = new JLabel("Вы уверены?");
                lblMessage.setFont(tnr);
                lblMessage.setBounds(120, 100, 380, 40);

                JLabel lblMessage2 = new JLabel("Отменить данное действие невозможно!");
                lblMessage2.setFont(tnr);
                lblMessage2.setBounds(5, 20, 395, 40);

                JButton btnYes = new JButton("Да");
                btnYes.setBounds(90, 450, 200, 40);

                JButton btnNo = new JButton("Отмена");
                btnNo.setBounds(90, 491, 200, 40);

                panel2.add(lblMessage);
                panel2.add(lblMessage2);
                panel2.add(btnYes);
                panel2.add(btnNo);
                panel2.setVisible(true);

                frame.revalidate();
                frame.repaint();
                frame.add(panel2);
                frame.setVisible(true);


                btnYes.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent actionEvent) {

//                        Map<String, Month> mountMap = monthsManager.deleteBusyDate(monthName, date);
                        Month mount = monthsManager.getMonthsMap().get(monthName.toUpperCase());
                        Map<Integer, Event> eventMap = month.getEventMap();
                        eventMap.remove(date);
                        month.setEventMap(eventMap);
                        monthsManager.getMonthsMap().put(monthName.toUpperCase(), month);
                        fileManager.createDataBaseFile(monthsManager.getMonthsMap());

                        frame.remove(panel2);
                        valueMenu = -1;
                        latch.countDown();
                    }
                });

                btnNo.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent actionEvent) {
                        frame.remove(panel2);
                        valueMenu = 0;
                        latch.countDown();

                    }
                });
            }
        });

        btnCancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                valueMenu = 0;
                frame.remove(panel);
                latch.countDown();
            }
        });

        frame.revalidate();
        frame.repaint();
        frame.setVisible(true);

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return valueMenu;
    }

    public String printBusyDates(String mountName) {
        String dates = "";

        ArrayList<Integer> busyDates = (ArrayList<Integer>) monthsManager.readBusyDate(mountName);
        ComparatorForInt comparator = new ComparatorForInt();

        Collections.sort(busyDates, comparator);
        for (Integer i : busyDates) {
            dates += " |" + i + "| ";
        }
        return dates;
    }

}
