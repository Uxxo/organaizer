package months;

public class EventManager {

    public EventManager(){
//        Map <Integer, Event> eventMap = new HashMap<>();
    }

    public Event createEvent(String type, String members,
                             String startedTime, String address, String cost, String specialInfo) {
        if (type.equals(null)) {
            type = " ";
        }
        if (members.equals(null)) {
            members = " ";
        }
        if (startedTime.equals(null)) {
            startedTime = " ";
        }
        if (address.equals(null)) {
            address = " ";
        }
        if (cost.equals(null)) {
            cost = " ";
        }
        if (specialInfo.equals(null)) {
            specialInfo = " ";
        }


        Event event = new Event();
        event.setType(type);
        event.setMembers(members);
        event.setStartedTime(startedTime);
        event.setAddress(address);
        event.setCost(cost);
        event.setSpecialInfo(specialInfo);

        return event;
    }


}
