package months;

import java.util.*;

public class Month {

    private String monthName;
    private int maxDate;
    Map<Integer, Event> eventMap = new HashMap<>();

    public Map<Integer, Event> getEventMap() {
        return eventMap;
    }

    public void setEventMap(Map<Integer, Event> eventMap) {
        this.eventMap = eventMap;
    }


    public Month(String mountName, int maxDate) {
        this.monthName = mountName;

        this.maxDate = maxDate;
    }


    public String getMonthName() {
        return monthName;
    }

    public int getMaxDate() {
        return maxDate;
    }

//    public List<Integer> getBusyDates() {
//        return busyDates;
//    }

//    public void setBusyDates(List<Integer> busyDates) {
//        this.busyDates = busyDates;
//    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Month month = (Month) o;
        return monthName == month.monthName;
    }

    @Override
    public int hashCode() {
        return Objects.hash(monthName);
    }

    @Override
    public String toString() {
        return "Month{" +
                "monthName='" + monthName + '\'' +
                ", maxDate=" + maxDate +
                ", eventMap=" + eventMap +
                '}';
    }
}
