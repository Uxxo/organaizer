package months;

import java.util.Objects;

public class Event {

    private String type;
    private String members;
    private String startedTime;
    private String address;
    private String cost;
    private String specialInfo;

    public Event() {
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMembers() {
        return members;
    }

    public void setMembers(String members) {
        this.members = members;
    }

    public String getStartedTime() {
        return startedTime;
    }

    public void setStartedTime(String startedTime) {
        this.startedTime = startedTime;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSpecialInfo() {
        return specialInfo;
    }

    public void setSpecialInfo(String specialInfo) {
        this.specialInfo = specialInfo;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Event event = (Event) o;
        return Objects.equals(type, event.type) &&
                Objects.equals(members, event.members);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, members);
    }

    @Override
    public String toString() {
        return "Event{" +
                "type='" + type + '\'' +
                ", members='" + members + '\'' +
                ", startedTime='" + startedTime + '\'' +
                ", address='" + address + '\'' +
                ", cost='" + cost + '\'' +
                ", specialInfo='" + specialInfo + '\'' +
                '}';
    }
}
