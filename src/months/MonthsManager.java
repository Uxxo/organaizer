package months;

import exceptions.InvalidDateException;

import java.util.*;


public class MonthsManager {

    private Map<String, Month> monthsMap;

    private Month month1 = new Month("ЯНВАРЬ", 31);
    private Month month2 = new Month("ФЕВРАЛЬ", 29);
    private Month month3 = new Month("МАРТ", 31);
    private Month month4 = new Month("АПРЕЛЬ", 30);
    private Month month5 = new Month("МАЙ", 31);
    private Month month6 = new Month("ИЮНЬ", 30);
    private Month month7 = new Month("ИЮЛЬ", 31);
    private Month month8 = new Month("АВГУСТ", 31);
    private Month month9 = new Month("СЕНТЯБРЬ", 30);
    private Month month10 = new Month("ОКТЯБРЬ", 31);
    private Month month11 = new Month("НОЯБРЬ", 30);
    private Month month12 = new Month("ДЕКАБРЬ", 31);


    public MonthsManager() {
        monthsMap = new HashMap<>();
        monthsMap.put(month1.getMonthName(), month1);
        monthsMap.put(month2.getMonthName(), month2);
        monthsMap.put(month3.getMonthName(), month3);
        monthsMap.put(month4.getMonthName(), month4);
        monthsMap.put(month5.getMonthName(), month5);
        monthsMap.put(month6.getMonthName(), month6);
        monthsMap.put(month7.getMonthName(), month7);
        monthsMap.put(month8.getMonthName(), month8);
        monthsMap.put(month9.getMonthName(), month9);
        monthsMap.put(month10.getMonthName(), month10);
        monthsMap.put(month11.getMonthName(), month11);
        monthsMap.put(month12.getMonthName(), month12);

        System.out.println("создается");
//        printMonthsMap();
    }


    public void printMonthsMap() {

        Set<String> keys = monthsMap.keySet();
        for (String s : keys) {
            Month month = monthsMap.get(s);
            System.out.println(month.toString());
            Map<Integer, Event> eventMap = month.getEventMap();
            Set<Integer> eventMapKeys = eventMap.keySet();
            for (Integer i : eventMapKeys) {
                Event event = eventMap.get(i);
                System.out.println(event.toString());
            }
        }
    }

    public void addDate(String mountName, int date) throws InvalidDateException {
        System.out.println("Дата: " + date);

        Month month = monthsMap.get(mountName.toUpperCase());
        Set<Integer> busyDates = month.eventMap.keySet();

        System.out.println(busyDates.toString());

        if (date < 1 || date > month.getMaxDate()) {
            throw new InvalidDateException("В этом месяце нет такой даты!");
        } else {
            if (busyDates.size()> 0 && busyDates.contains(date)) {
                throw new InvalidDateException("Эта дата уже занята!");
            } else {
                Event event = new Event();
                month.getEventMap().put(date, event);
                monthsMap.put(mountName.toUpperCase(), month);
//                mount.getBusyDates().add(date);
//                montsMap.put(mount.getMonthName(), mount);

            }
        }
    }

    public void refreshMonthsMap(Map<String, Month> newMonthMap) {

        monthsMap = newMonthMap;


    }

    public List readBusyDate(String montName) {
        List<Integer> busyDate = new ArrayList<>();
        Month month = monthsMap.get(montName.toUpperCase());
        Set<Integer> eventDate = month.eventMap.keySet();
        for (Integer i : eventDate) {
            busyDate.add(i);
        }

        return busyDate;
    }

    public Map<String, Month> deleteBusyDate(String monthName, int date) {
        Month month = monthsMap.get(monthName.toUpperCase());
        Map<Integer, Event> eventMap = month.getEventMap();

        eventMap.remove(eventMap.get(date));
        month.setEventMap(eventMap);

//        List<Integer> busyDate = mount.getBusyDates();

//        int index = busyDate.indexOf(date);
//        busyDate.remove(index);
//        mount.setBusyDates(busyDate);
        monthsMap.put(monthName, month);


        return monthsMap;
    }

    public Month readMount(String mountsName) {

        Month month = monthsMap.get(mountsName.toUpperCase());
        return month;
    }

    public Map<String, Month> getMonthsMap() {

        return monthsMap;
    }

    public void setMonthsMap(Map<String, Month> monthsMap) {
        this.monthsMap = monthsMap;
    }
}


