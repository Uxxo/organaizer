import months.Month;
import months.MonthsManager;

import java.util.Map;

public class Application {
    private FileManager fileManager = new FileManager();
    private Month month;

    private View view = new View();

    private boolean isPassword = false;
    private boolean isReturn = false;


    public void start() {

        int dateEvent;
        int valueMenu;

        String input = null;
        String password = fileManager.readPassword(fileManager.getPasswordFileName());
        while (1 > 0) {
            if (password == null) {
                while (!isPassword) {
                    isPassword = view.fillOnPasswordForm();
                    if (isPassword){
                        password = fileManager.readPassword(fileManager.getPasswordFileName());
                    }
                    break;
                }
            }

            if (password != null) {
                while (!isPassword) {
                    input = view.singIn();
                    isPassword = checkPassword(Integer.parseInt(password), input.hashCode());
                    if (!isPassword) {
                        view.showMessageInvalidPassword();
                    }
                }
                while (isPassword) {
                    isReturn = false;
                    System.out.println("Зашли в старт");
                    String montName = view.start();  // Меню выбора месяца.
                    while (!isReturn) {


                        valueMenu = view.getMount(montName); // Меню выбора даты.
                        dateEvent = valueMenu;

                        if (valueMenu == 100) { // Добавить дату
                            dateEvent = view.addDate(montName);
                            if (dateEvent == 0) {
                                break;
                            }
                            view.createNewEvent(montName, dateEvent); // Создать мероприятие
                            break;
                        }
                        if (valueMenu == 0) {
                            break;
                        } else {
                            valueMenu = view.openEvent(montName, dateEvent); // Открыть мероприятие
                            switch (valueMenu) {
                                case 0:
                                    break;
                                case 1:
                                    valueMenu = view.editEvent(montName, dateEvent); // Редактировать мероприятие
                                    switch (valueMenu) {
                                        case 0:
                                            break;
                                        case -1:
                                            valueMenu = view.deleteEvent(montName, dateEvent); // Удалить мероприятие
                                            switch (valueMenu) {
                                                case 0:
                                                    break;
                                                case -1:
                                                    isReturn = true;

                                            }
                                    }
                            }

                        }
                    }
                }
            }

        }
    }


//    public void loadFiles() {
//        Map<String, Month> loadingMap;
////        readMap(mountsManager.getMountsMap());
//
//        loadingMap = fileManager.readDataBaseFile(monthsManager.getMontsMap());
//        System.out.println("===============================================");
////        readMap(loadingMap);
//        monthsManager.setMontsMap(loadingMap);
//        System.out.println("===============================================");
////        readMap(mountsManager.getMountsMap());
//    }

//    public void readMap(Map<String, Month> map) {
//        Set<String> keys = map.keySet();
//        for (String s : keys) {
//            Month month = map.get(s);
//            System.out.println(month.getBusyDates().toString() + "=========" + s);
//        }
//    }

    public boolean checkPassword(int passwordHash1, int passwordHash2) {
        boolean flag;
        if (passwordHash1 == passwordHash2) {
            flag = true;
        } else {
            flag = false;
        }
        return flag;
    }

}
